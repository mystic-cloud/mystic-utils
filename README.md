**Illinois Institute of Technology**  
**Data-Intensive Distributed Systems Laboratory**  
**Mystic Cloud Utils**  
Maintainer: Alexandru Iulian Orhean (aorhean@hawk.iit.edu)

This project contains scripts and small programs for interacting with the Mystic Cloud internal resources.

# How to mount your Mystic home directory (and your project directory) on a compute instance

## Requirements

- your instance needs to be connected to **shared-net**;
- your instance needs to run one of the MC-Ubuntu18.04\* images;
- you need to be able to SSH to the login nodes from any login node without password (generate on the login node a RSA 
key pair and copy the contents of the public key in ~/.ssh/authorized\_keys also on the login node)

## Before you run the script

Before you run the script you need to create two files (**user.cfg** and **projects.cfg**) containing the name and 
id of your user on Mystic, and the name and id of the project on Mystic if you want to mount the project directory 
too. If you do not have a project directory or if you do not want to mount your project directory then just pass an 
empty **projects.cfg** file. You will also need to retrieve your Ceph secret key from 
**~/.secrets/<user name>.secret**.

The **user.cfg** file has the following structure:
```
<user name> <user id>
```

The **projects.cfg** file has the following structure:
```
<project 1 name> <project 1 id>
<project 2 name> <project 2 id>
...
<project n name> <project n id>
```

You can retreive the user name, user id, project name and project id information by running **id**. Here is an example:
```
aorhean@login1:~$ id
uid=2000(aorhean) gid=2000(aorhean) groups=2000(aorhean),1998(xsearch),1999(mcusers)
```

In the example my username is **aorhean**, my user id is **2000**, my project name is **xsearch** and my project id is 
**1998**.

Here is an example on how to retrieve the Ceph secret key:
```
aorhean@login1:~$ cat .secrets/aorhean.secret
AXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==
```

## Running the script

You will need to run the script as root. When you run the script for the first time it will install the ceph client 
packages, but afterwards you can run the script any time your need to mount your home directory (and you project
directories).

Here is a detailed example on how to run the script, including how to clone this repository:
```
ubuntu@demo-instance:~$ git clone https://gitlab.com/mystic-cloud/mystic-utils.git
ubuntu@demo-instance:~$ cd mystic-utils
ubuntu@demo-instance:~/mystic-utils$ ls -l
total 20
-rw-rw-r-- 1 ubuntu ubuntu 1069 Jun 11 21:08 LICENSE
-rw-rw-r-- 1 ubuntu ubuntu  299 Jun 11 21:08 README.md
drwxrwxr-x 2 ubuntu ubuntu 4096 Jun 11 21:08 mysticfs
-rw-rw-r-- 1 ubuntu ubuntu   13 Jun 11 21:08 projects.cfg
-rw-rw-r-- 1 ubuntu ubuntu   13 Jun 11 21:08 user.cfg
ubuntu@demo-instance:~/mystic-utils$ cat user.cfg
aorhean 2000
ubuntu@demo-instance:~/mystic-utils$ cat projects.cfg
xsearch 1998
ubuntu@demo-instance:~/mystic-utils$ sudo ./mysticfs/mount-fs.sh user.cfg projects.cfg AXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX==
ubuntu@demo-instance:~/mystic-utils$ df -h
Filesystem                     Size  Used Avail Use% Mounted on
udev                           6.9G     0  6.9G   0% /dev
tmpfs                          1.4G  700K  1.4G   1% /run
/dev/sda1                      277G  2.4G  263G   1% /
tmpfs                          6.9G     0  6.9G   0% /dev/shm
tmpfs                          5.0M     0  5.0M   0% /run/lock
tmpfs                          6.9G     0  6.9G   0% /sys/fs/cgroup
tmpfs                          1.4G     0  1.4G   0% /run/user/1000
172.20.0.34:/home/aorhean      286T   22T  265T   8% /exports/home/aorhean
172.20.0.34:/projects/xsearch  286T   22T  265T   8% /exports/projects/xsearch
```

After running the script succesfully you should be able to SSH to your instance from the login node using your username:
```
aorhean@login1:~$ ssh -i .ssh/mystic aorhean@<instance ip address>
aorhean@demo-instance:~$ ls -l /exports/home/aorhean/
aorhean@demo-instance:~$ ls -l /exports/projects/
```

