#!/bin/bash

function install_ceph_client {
    rc=$(apt list --installed | grep "ceph-common" | wc -l)
    if [ $rc -eq 0 ]
    then
        apt -y install ceph-common
    fi
}

function create_user {
    user=$1
    userid=$2
    rc=$(cat /etc/passwd | grep "^$user:*" | wc -l)
    if [ $rc -eq 0 ]
    then
        useradd -m -b /home -s /bin/bash -u $userid $user
    fi
}

function create_group {
    group=$1
    groupid=$2
    rc=$(cat /etc/group | grep "^$group:*" | wc -l)
    if [ $rc -eq 0 ]
    then
        groupadd -g $groupid $group
    fi
}

function mount_user_home {
    user=$1
    key=$2
    if [ ! -d /exports/home/$user ]
    then
        mkdir /exports/home/$user
        chown $user:$user /exports/home/$user
        chmod 770 /exports/home/$user
    fi
    rc=$(df -h | grep "/exports/home/$user" | wc -l)
    if [ $rc -eq 0 ]
    then
        mount -t ceph 172.20.0.34:/home/$user /exports/home/$user -o name=$user,secret=$key
    fi
    rc=$(df -h | grep "/exports/home/$user" | wc -l)
    if [ $rc -ne 0 ]
    then
        mkdir /home/$user/.ssh
        chmod 700 /home/$user/.ssh
        cp /exports/home/$user/.ssh/authorized_keys /home/$user/.ssh/.
        chown -R $user:$user /home/$user/.ssh
    fi
}

function mount_project {
    user=$1
    project=$2
    key=$3
    if [ ! -d /exports/projects/$project ]
    then
        mkdir /exports/projects/$project
        chown root:$project /exports/projects/$project
        chmod ug+rwxs /exports/projects/$project
        chmod o-rwx /exports/projects/$project
    fi
    rc=$(df -h | grep "/exports/home/$project" | wc -l)
    if [ $rc -eq 0 ]
    then
        mount -t ceph 172.20.0.34:/projects/$project /exports/projects/$project -o name=$user,secret=$key
    fi
}

function add_user_to_group {
    user=$1
    group=$2
    rc=$(cat /etc/group | grep "^$group:*" | grep "^$user:*" | wc -l)
    if [ $rc -eq 0 ]
    then
        adduser $user $group
    fi
}

if [ $# -ne 3 ]
then
    echo "USAGE: sudo ./mount-fs.sh <user file> <projects file> <ceph key>"
fi

user_file=$1
projects_file=$2
key=$3

users=()
userids=()
projects=()
projectids=()

while read line
do
    line_array=($line)
    users+=(${line_array[0]})
    userids+=(${line_array[1]})
done < $user_file

while read line
do
    line_array=($line)
    projects+=(${line_array[0]})
    projectids+=(${line_array[1]})
done < $projects_file

if [ ${#users[@]} -eq 0 ] || [ ${#userids[@]} -eq 0 ]
then
    echo "ERROR: the user file contains no user entry!"
    exit 1
fi

for((i=0;i<${#users[@]};i++))
do
    user=${users[$i]}
    userid=${userids[$i]}

    install_ceph_client
    if [ ! -d /exports ]
    then
        mkdir /exports
        mkdir /exports/home
        mkdir /exports/projects
    fi
    
    create_user $user $userid
    mount_user_home $user $key
done

for((i=0;i<${#projects[@]};i++))
do
    project=${projects[$i]}
    projectid=${projectids[$i]}
    
    create_group $project $projectid
    for((i=0;i<${#users[@]};i++))
    do
        user=${users[$i]}
        userid=${userids[$i]}
        add_user_to_group $user $project
    done
    mount_project $user $project $key
done

